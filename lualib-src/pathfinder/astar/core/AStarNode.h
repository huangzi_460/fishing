#ifndef Node_h__
#define Node_h__

#include <stdlib.h>
#include <string.h>
#include "define.h"



typedef struct st2Int
{
	int x;
	int y;
}AStarPoint;

class AStarNode
{
public:
	AStarNode(void):parent(),x(),y(),f(),g(),h(),opened(),closed()
	{
	}

	~AStarNode(void){}

	bool init(int _x, int _y, bool block)
	{
		x = _x;
		y = _y;
		isBlock = block;
		return true;
	}

	void reset()
	{
		parent = NULL;
		f = g = h = 0.0f;
		opened = closed = false;
	}

public:
	AStarNode* parent;
	int x;	//  the x-coordinate of the node on the collision map
	int y;	//  the y-coordinate of the node on the collision map
	float f;	// F-cost
	float g;
	float h;
	bool opened;
	bool closed;
	bool isBlock;


};

#endif // Node_h__
