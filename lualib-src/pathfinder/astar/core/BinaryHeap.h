#ifndef BinaryHeap_h__
#define BinaryHeap_h__

#include "AStarNode.h"
#include <vector>

using namespace std;

class BinaryHeap
{
public:
	BinaryHeap(void);
	~BinaryHeap(void);

	void percolate_up(int idx);

	void percolate_down(int idx);

	bool empty();

	void clear();

	void push(AStarNode* n);

	AStarNode* pop();

	void heapify(AStarNode* n);

	void swap(int idx1,int idx2);

private:
	//map<int,Node*> heap;
	std::vector<AStarNode*> heap;
	int hsize;
	
};

#endif // BinaryHeap_h__
