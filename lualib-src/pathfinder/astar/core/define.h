#ifndef define_h__
#define define_h__

typedef signed char         int8;
typedef signed short        int16;
typedef signed int          int32;
typedef unsigned char       uint8;
typedef unsigned short      uint16;
typedef unsigned int        uint32;

#define Huge 65536


#endif // define_h__
