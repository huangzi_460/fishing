#ifndef __HEURISTICS_H__
#define __HEURISTICS_H__

#include <math.h>
#include "AStarNode.h"

#define MAX(a,b) (a)>(b)?(a):(b)
#define MIN(a,b) (a)>(b)?(b):(a)

#define sqrt2 1.414f

inline int manhattan(AStarNode* nodeA, const AStarNode* nodeB)
{
	int dx = abs(nodeA->x - nodeB->x);
	int dy = abs(nodeA->y - nodeB->y);
	return dx + dy;
}

inline float euclidian(AStarNode* nodeA,AStarNode* nodeB)
{
	int dx = nodeA->x - nodeB->x;
	int dy = nodeA->y - nodeB->y;
	return sqrt((float)(dx*dx+dy*dy));
}

inline int diagonal(AStarNode* nodeA, AStarNode* nodeB)
{
	int dx = abs(nodeA->x - nodeB->x);
	int dy = abs(nodeA->y - nodeB->y);
	return MAX(dx,dy);
}

inline int cardintcard(AStarNode* nodeA, AStarNode* nodeB)
{
	int dx = abs(nodeA->x - nodeB->x);
	int dy = abs(nodeA->y - nodeB->y);
	return MIN(dx,dy) * sqrt2 + MIN(dx,dy) - MIN(dx,dy);
}

#endif