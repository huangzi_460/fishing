#ifndef AStar_h__
#define AStar_h__

#include <vector>

class AStarNode;
class PathFinder;
class BinaryHeap;

class AStar
{
public:
	AStar(void);
	virtual ~AStar(void);

	virtual void computeCost(AStarNode* node,AStarNode* neighbour,PathFinder* finder);
	
	virtual void updateVertex(PathFinder* finder,BinaryHeap* openList,AStarNode* node,AStarNode* neighbour,AStarNode* endNode);

	virtual AStarNode* calculatePath(PathFinder* finder,AStarNode* startNode,AStarNode* endNode,std::vector<AStarNode*>& toClear);

protected:
	BinaryHeap* openList;

};

#endif // AStar_h__
