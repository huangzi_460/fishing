------------------------------------------------------
-- 这是基于一个loop模式的定时器
-- by ChengZhe          -- 2015-5-21
------------------------------------------------------
require("functions")

-- 简单的定时器类
local CMyTimer = class("CMyTimer")


function CMyTimer:ctor()
    self._beginTime = nil -- 定时器开始时间
    self._trueTime = nil  -- 定时器间隔时间
    self._oldTime = nil   -- 定时器上次被Tick时间
end


-- 开启定时器
function CMyTimer:beginTimer(expireTime, nowACTime)
    self._beginTime = nowACTime
    self._trueTime = expireTime
    self._oldTime = nowACTime
end

-- 停用定时器
function CMyTimer:stopTimer()
   self._trueTime = nil
end

-- 检测定时器是否停用
function CMyTimer:isStop()
     return not self._trueTime
end

-- 判断定时器是否命中 (需要在Tick中判断)
function CMyTimer:isTimerExpire(acTime)
    if self:isStop() then
        return false
    end

    if (acTime < self._oldTime + self._trueTime ) then
        return false
    end

    self._oldTime = acTime;
    return true;
end


return CMyTimer



