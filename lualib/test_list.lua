local ListCtrl = require("list")
local function printFunc(node)
    print ("node = ", node.data)
end


local list = ListCtrl.new()

list:push_front(4)
list:push_front(3)
list:push_front(2)
list:push_front(1)


list:push_back(5)
list:push_back(6)
list:push_back(7)
list:push_back(8)
list:_debug_printAllNode(printFunc)

print ("------pop front begin-----")
print(list:pop_front_data())
print(list:pop_front_data())
print ("------pop front end-----")
list:_debug_printAllNode(printFunc)
print ("------pop back begin-----")
print(list:pop_back_data())
print(list:pop_back_data())
print ("------pop back end-----")
list:_debug_printAllNode(printFunc)

local findNode = list:find(function(node) return node.data end, 4)
print ("------- find Node-----")
print (findNode.data)
print ("------- remove Node begin-----")
list:remove_node(findNode)
print ("------- remove Node end-----")
list:_debug_printAllNode(printFunc)

print ("--------clear test----------")
for i = 1, list:getNodeCount() do
    -- list:pop_front_data()
    list:pop_back_data()
end
list:_debug_printAllNode(printFunc)
print ("--------clear end----------")

print ("--------out of range test begin------")
list:pop_back_data()
list:pop_front_data()
list:pop_back_data()
list:pop_front_data()
list:pop_back_data()
list:pop_front_data()

list:_debug_printAllNode(printFunc)
print ("--------out of range test end------")

print (list.pop_front_data)